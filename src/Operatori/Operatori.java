/* Создать программу которае решает проблему
Если время от 8 до 12 и денег больше 10 то идем в магазин,
если время больше 12 и денег больше 50 идем в кафе
если денег меьше 50 и время меньше 19 то идем к соседу,
если  время больше 19 и меньше 22 то смотрим телевизор,
если больше 22 то идем спать
Результат выводим в консоль */

package Operatori;

import java.util.Scanner;

public class Operatori {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите, пожалуйста, время от 0 до 23 часов.");
        int time = in.nextInt();
        Scanner in2 = new Scanner(System.in);
        System.out.println("Сколько у тебя денег?");
        int money = in2.nextInt();
        if (8 <= time && time <= 12 && money >= 10) {
            System.out.println("Бежим в магазин скорее!");
        } else if (12 < time && time <= 19 && money >= 50) {
            System.out.println("Пошли в кафе.");
        } else if (12 < time && time <= 19 && money < 50) {
            System.out.println("Придется идти к соседу.");
        } else if (19 < time && time <= 22 && money >= 0) {
            System.out.println("Смотрим телевизор.");
        } else if (22 < time && time <= 23 && money >=0) {
            System.out.println("Ложись спать!");
        } else if (0 <= time && time < 8 && money >=0) {
            System.out.println("Ложись спать!");
        } else System.out.println("Введи корректные данные!");
    }
}