/*      1. Создать два класса и реализоввать в первом классе 3 метода с модификатором
        паблик прайфвет и протектет
        2.Унаследовать класс 1 классом 2 и вызвать  методы с модификатором паблик и протектет
        3.Создать клас 3 вызвать метод с модификатором паблик и с модификатором пекдж прайвет
        4ю Создать паблик статик метод и  вызвать его в другом классе */

package Modificatori;

public class First {

    public int summ(int a, int b) {
        return a + b;
    }

    private String word(String a) {
        return a;
    }

    protected int mult(int a, int b) {
        return a * b;
    }

    int div(int a, int b) {
        return a / b;
    }

    public static int subst(int a, int b) {
        return a - b;
    }
}
