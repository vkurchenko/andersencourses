/* 3. Сделать оработку исключений с nullpointer и fileIO */

package Exception;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileIO {
    public static void main(String[] args) {
        File news = new File("News");
        try {
            Scanner in = new Scanner(news);
            System.out.println("Все гуд, мэн!"); //Если файл найден.
        } catch (FileNotFoundException f) {
            System.out.println("Файл с таким именем не был найден. Сорян."); //Если файл не найден
        }
    }
}
