/* 3. Сделать оработку исключений с nullpointer и fileIO */

package Exception;

import java.util.Scanner;

public class NullPointer {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите слово: ");
        String name = in.nextLine();
        if (name.isEmpty()) {
            name = null;
        }
        try {
            System.out.println(name.length());
        } catch (Exception nu) {
            System.out.println("В переменной пусто.");
        }
    }
}
