/* 2. Проверить конструкцию без catch с final */

package Exception;

import java.util.Scanner;

public class WithoutCatch {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите первое число, которое будете делить: ");
        int n = in.nextInt();
        System.out.print("Введите второе число, на которое будете делить: ");
        int v = in.nextInt();

        try {
            System.out.println("Частное: " + n / v);
        } finally {
            System.out.println("Я вас категорически поздравляю!");
        }
    }
}