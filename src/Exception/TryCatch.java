/* 1. Завернуть в try catch 1/0 и вывести на ноль делить нельзя */

package Exception;

import java.util.Scanner;

public class TryCatch {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите первое число, которое будете делить: ");
        int n = in.nextInt();
        System.out.print("Введите второе число, на которое будете делить: ");
        int v = in.nextInt();

        try {
            System.out.println("Частное: " + n/v);
        }catch (Exception ex){
            System.out.println("На ноль делить нельзя!");
        }
    }
}