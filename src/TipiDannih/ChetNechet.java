/* 5. Вывести на экран информацию о том является ли целое
число записанное в переменную n, чётным либо нечётным.*/

package TipiDannih;
import java.util.Scanner;

public class ChetNechet {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите, пожалуйста, число, которое");
        System.out.print("хотите проверить - четное оно или нет: ");
        int number = in.nextInt();

        if (number%2 == 0 && number != 0) {
            System.out.println("Введенное число четное.");
        }else if (number == 0) {
            System.out.println("Вы ввели число 0.");
        }else{
            System.out.println("Введенное число нечетное.");
        }
    }
}