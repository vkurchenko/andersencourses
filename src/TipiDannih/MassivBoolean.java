/* 11. Преобразовать числовой массив в boolean */

package TipiDannih;

public class MassivBoolean {
    public static void main(String[] args) {
        int num[] = new int[]{1, 0, 1};
        boolean[] bool = new boolean[num.length];
        for (int i = 0; i < bool.length; i++) {
            if (num[i] == 1) {
                bool[i] = true;
            } else if (num[i] == 0) {
                bool[i] = false;
            }
            System.out.println(bool[i]);
        }
    }
}
