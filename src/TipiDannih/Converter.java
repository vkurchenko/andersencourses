/* 9. Перобразовать массив в строку и обратно в массив. */

package TipiDannih;

import java.util.Arrays;
import java.util.Scanner;

public class Converter {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Введите длину массива: ");
        int sizeArray = input.nextInt();
        int[] array = new int[sizeArray];
        System.out.println("Введите элементы массива:");
        for (int i = 0; i < array.length; i++) {
            array[i] = input.nextInt();
        }
        String str = Arrays.toString(array);
        System.out.println("Массив строкой: " + str);
        String[] numArr = str.split(" ");
    }
}
