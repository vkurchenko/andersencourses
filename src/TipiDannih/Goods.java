/* 4. Вывести цену за определенный товар опираясь на его код
        (предусмотреть возможность введение неверного кода). */
package TipiDannih;
import java.util.Scanner;

public class Goods {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите, пожалуйcта, код товара, цену которого вы хотите узнать: ");
        System.out.println("- Aвтомобиль: 101;");
        System.out.println("- Столовый прибор: 102;");
        System.out.println("- Картошка: 203.");
        int code = in.nextInt();

        switch (code) {
            case 101:
                System.out.println("Cтоимоcть: ~25000$");
                break;
            case 102:
                System.out.println("Cтоимоcть: 100р.");
                break;
            case 203:
                System.out.println("Cтоимоcть: 450BYN");
                break;
            default:
                System.out.println("Товара c данным кодом не найдено.");
                break;
        }
    }
}
