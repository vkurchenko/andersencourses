/* 3. Факториал числа. */

package TipiDannih;
import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите номер: ");
        int number = in.nextInt();
        int result = 1;
        for (int i = 1; i <= number; i++){
            result = result * i;
        }
        System.out.println("Факториал числа: " + result);
    }
}
