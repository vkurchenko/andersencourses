/* 2. Посчитать среднее арифметическое массива. */

package TipiDannih;

public class Average {
    public static void main(String[] args) {
        int[] numbers = {1, 4, 4, 5, 9};
        int result = 0;
        for (int i = 0; i < numbers.length; i++) {
            result = result + numbers[i];
        }
        result = result / numbers.length;
        System.out.println("Среднее арифметическое: " + result);
    }
}
