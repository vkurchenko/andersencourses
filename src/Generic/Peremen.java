/* 1. Создать переменную с определенным типом
   2. Создать метод с возможностью генерирования где будем менять тип  данных для генерации */

package Generic;

public class Peremen {
    public static void main(String[] args) {
        int e = 5;
        String t = "Текст";
        Typetester ttt = new Typetester();
        Gen<Integer> gen = new Gen<>();
        gen.getE(e);
        gen.getE(t);
        ttt.printType(e);
        ttt.printType(t);
    }
}