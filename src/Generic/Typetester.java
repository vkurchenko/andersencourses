package Generic;

public class Typetester {
    void printType(byte x) {
        System.out.println(x + " это byte.");
    }

    void printType(int x) {
        System.out.println(x + " это int.");
    }

    void printType(float x) {
        System.out.println(x + " это float.");
    }

    void printType(double x) {
        System.out.println(x + " это double.");
    }

    void printType(char x) {
        System.out.println(x + " это char.");
    }

    void printType(String x) {
        System.out.println(x + " это string.");
    }

}
