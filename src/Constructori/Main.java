/* 1. Создать дефолтный коструктор в котором будет выводится имя класса
2. Создать второй конструктор в котором будет выводится Имя класса и названия того кто его делал */

package Constructori;

public class Main {

    public static void main(String[] args) {

        Car Honda = new Car();
        Car Bmw = new Car(45, 102, "Ласточка", "Вин Дизель");
    }
}

class Car {

    private int speed;
    private int weight;
    private String name;
    private String creator;

    Car() {
        System.out.println("Просто имя класса: " + getClass().getSimpleName() + ".");
        System.out.println(" ");
    }

    Car(int speed, int weight, String name, String creator) {
        this.speed = speed;
        this.weight = weight;
        this.name = name;
        this.creator = creator;
        System.out.println("Вывод введенных параметров:");
        System.out.println("- Максимальная скорость: " + speed + " км/ч.");
        System.out.println("- Имя красавицы твоей: " + weight + " кг.");
        System.out.println("- Имя красавицы твоей: " + name + ".");
        System.out.println("- Имя создателя: " + creator + ".");
    }
}