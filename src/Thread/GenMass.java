/* 1.Cоздать синхронизированый метод который будет использоватся в 2 разных потоках */

package Thread;

import java.util.*;

public class GenMass {

    public static void main(String[] args) {

        Thread t1 = new Thread();
        Thread t2 = new Thread();
        t1.start();
        t2.start();
    }
}

class Thread extends java.lang.Thread {

    private Random r = new Random();
    private int param = r.nextInt(9999);

    public void run() {
        System.out.println("Рандомное число: " + param + ".");
    }
}