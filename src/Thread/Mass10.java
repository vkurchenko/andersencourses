/* 2.Создать метод которы будет в паралеле создавать массивы из 5 рандомных значений
количество потоков 10
Генерация данных  происходит параллельно, используется один и тот же метод
метод возвращает разные значения каждому из потоков */

package Thread;

import java.util.Arrays;
import java.util.Random;

public class Mass10 {
    public static void main(String[] args) {

        Thread10 t1 = new Thread10();
        Thread10 t2 = new Thread10();
        Thread10 t3 = new Thread10();
        Thread10 t4 = new Thread10();
        Thread10 t5 = new Thread10();
        Thread10 t6 = new Thread10();
        Thread10 t7 = new Thread10();
        Thread10 t8 = new Thread10();
        Thread10 t9 = new Thread10();
        Thread10 t10 = new Thread10();
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();
        t9.start();
        t10.start();
    }
}

class Thread10 extends java.lang.Thread {

    int[] array = new int[5];

    public void run() {
        for (int i = 0; i < array.length; i++) {
            Random r = new Random();
            int num = r.nextInt(20);
            array[i] = num;
        }
        String str = Arrays.toString(array);
        System.out.println("Массив строкой: " + str + ".");
    }
}