/* 1.Создать hashMap и добавить в него несколько  обьектов
2.Вывести в консоль все данные */

package HashMap;

import java.util.*;

public class HashMap {

    public static void main(String[] args) {

        java.util.HashMap<String, Integer> hashMap = new java.util.HashMap<>();
        hashMap.put("age", 25);
        hashMap.put("weight", 50);
        hashMap.put("hands", 2);
        hashMap.put("legs", 2);

        Set<String> strings = hashMap.keySet();
        for (String s : strings) {
            System.out.println(s);
        }

        for (Integer i : hashMap.values()) {
            System.out.println(i);
        }
        /* 3.Создать set и передать в него 2 одинаковых значения */

        Set<String> set = new HashSet<String>();
        set.add("First.");
        set.add("Second.");
        set.add("First.");

        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
