/*
1.Создать интерфейс звери в нем создать методы
рост
вес
движение
есть
*/

package OOP;

public class OOP {

    public interface Animal{
        int getGrowth();
        int getWeight();
        String getMotion();
        String getEat();
    }

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    /* 2.Создать класс собака и  имплементировть интерфейс звери */

    public class Sobaka implements Animal {

        public int getGrowth() {
            return 50;
        }

        public int getWeight() {
            return 20;
        }

        public String getMotion() {
            return "Вперед!";
        }

        public String getEat() {
            return "Сухой корм";
        }
    }

    /* 3. Создать класс дог и переопределить  в нем основые методы */

    public class Dog extends Sobaka {

        public int getGrowth() {
            return 80;
        }

        public int getWeight() {
            return 40;
        }

        public String getMotion() {
            return "Уходи в бок!";
        }

        public String getEat() {
            return "Питательный корм.";
        }
    }

    /* 4. Перегрузить метод рост вес и движение с разыми параметрами
    (футы,метры,метрические еденицы ...) */

    public class Dog2 extends Sobaka {

        public int getGrowth() {
            return 80;
        }

        public int getGrowth(int i) {
            return i;
        }

        public float getGrowth(float f) {
            return f;
        }

        public int getWeight() {
            return 40;
        }

        public int getWeight(int i) {
            return i;
        }

        public double getWeight(double d) {
            return d;
        }

        public String getMotion() {
            return "Уходи в бок!";
        }

        public String getMotion(String a) {
            return a;
        }
    }
}