/* 2. Создать цикл с послеусловием и с перед условием который будет выводить число а, пока оно не станет меньше 10 */

package Cikli;

public class While {
    public static void main(String[] args) {

        int a1 = 21;
        do {
            a1--;
            System.out.println(a1);
        }while (a1 > 10);
        System.out.println(" ");

        int a2 = 20;
        while(a2 >= 10) {
            System.out.println(a2);
            a2--;
        }
    }
}
