/* 3. Создать цикл foreach  для массива из 10 рандомных чисел,если число равно 5 вывести в консоль */

package Cikli;

import java.util.Random;

public class Foreach {
    public static void main(String[] args) {

        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            Random r = new Random();
            int num = r.nextInt(10);
            array[i] = num;
            //System.out.println(array[i]);   //Можно выключить комментарий для проверки цифр в массиве.
        }

        for (int i : array) {
            if (i != 5) {
                continue;
            } else System.out.println("В массиве встретилось число: " + i + ".");
        }
    }
}
