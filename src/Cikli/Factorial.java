/* 1. Создать метод который будет работать как факториал */

package Cikli;

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите число: ");
        int n = in.nextInt();

        if (n == 0) {
            System.out.println("Вы ввели 0, введите положительное число.");
        } else if (n > 0) {
            System.out.println(factorial(n));
        } else System.out.println("Введите положительное число.");

        System.out.println(" ");
    }

    static int factorial(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }
}
